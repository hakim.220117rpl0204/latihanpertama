var moment = require('moment');

format1 = moment().format();
// 2019-07-28T14:50:32+07:00
format2 = moment().format('dddd');
// sunday
format3 = moment().format('MMM Do YY');
// Jul 28th 19
format4 = moment().format('YYYY [escaped] YYYY');
// 2019 escaped 2019
format5 = moment().format('MMMM Do YYYY, h:mm:ss a');
// July 28th 2019, 2:49:40 pm

console.log(format5);